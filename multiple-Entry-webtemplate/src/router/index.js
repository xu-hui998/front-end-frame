// eslint-disable-next-line no-undef
Vue.use(VueRouter)

const login = (resolve) => {
  import('../views/login.vue').then((module) => {
    resolve(module)
  })
}
// eslint-disable-next-line no-undef
export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'login',
      component: login,
    },
  ],
})
