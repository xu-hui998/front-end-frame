// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import Vue from 'vue';
import App from './App.vue';
import router from './router';

// eslint-disable-next-line no-undef
Vue.prototype.axios = axios;
// eslint-disable-next-line no-new, no-undef
new Vue({
  el: '#app1',
  router,
  components: { App },
  template: '<App/>',
});
