// eslint-disable-next-line no-undef
Vue.use(VueRouter);

const HelloWorld = (resolve) => {
  import('../views/HelloWorld.vue').then((module) => {
    resolve(module);
  });
};

// eslint-disable-next-line no-undef
export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld,
    },
  ],
});
