/**
* @Description: 公共loading组件
* @Author: luhan
* @Date: 2021/02/01 16:38:31
* @param: none
* @return: none
*/
function initContianer() {
  const container = document.createElement('div');
  container.style.position = 'fixed';
  container.style.top = '0px';
  container.style.left = '0px';
  container.style.height = '100vh';
  container.style.width = '100%';
  container.style.backgroundColor = 'rgba(255, 255, 255, 0.3)';
  container.style.overflow = 'hidden';
  container.style.zIndex = '99999';

  return container;
}
function initLoad() {
  const div = document.createElement('div');
  // div.style.height = '45px';
  div.style.width = '100%';
  div.style.boxSizing = 'border-box';
  div.style.position = 'absolute';
  div.style.top = '43%';
  div.style.textAlign = 'center';

  const ul = document.createElement('ul');
  ul.style.margin = '0px';
  ul.style.padding = '0px';
  ul.style.listStyleType = 'none';
  ul.style.position = 'absolute';
  ul.style.borderRadius = '50%';
  ul.style.height = '45px';
  ul.style.width = '45px';
  ul.style.left = 'calc(50% - 25px)';
  div.appendChild(ul);
  for (let index = 0; index < 12; index += 1) {
    const li = document.createElement('li');
    li.setAttribute('index', index);
    li.style.margin = '0px';
    li.style.padding = '0px';
    li.style.listStyleType = 'none';
    li.style.position = 'absolute';
    li.style.left = '50%';
    li.style.borderRadius = '50%';
    li.style.height = '3.37px';
    li.style.width = '3.37px';
    li.style.backgroundColor = '#009A63';
    li.style.transform = `rotate(${index * 30}deg)`;
    li.style.transformOrigin = '2.5px 17.5px';
    li.style.opacity = '0.2';
    ul.appendChild(li);
  }
  return div;
}
class Loading {
  constructor() {
    this.beShow = false;
    this.container = null;
    this.handle = null;
    this.handleText = null;
    this.loadingText = null;
    this.accumulator = 0;
  }

  show() {
    // 累加器加一
    this.accumulator += 1;
    if (this.beShow && this.accumulator !== 1) {
      return false;
    }

    // 防止整屏滚动
    document.body.style.overflow = 'hidden';
    this.container = initContianer();
    let loadingContainer = initLoad();
    this.container.appendChild(loadingContainer);
    this.handleText = setTimeout(() => {
      let loadingText = document.createElement('p');
      loadingText.style.color = '#666666';
      loadingText.style.marginTop = '70px';
      loadingText.style.display = 'inline-block';
      loadingText.innerText = '正在努力加载，请耐心等待';
      loadingContainer.appendChild(loadingText);
    }, 5000);
    document.body.appendChild(this.container);
    this.animationIndex = 0;
    let ul = loadingContainer.children[0];
    this.handle = setInterval(() => {
      // 重置所有
      for (let index = 0; index < 12; index += 1) {
        ul.children[index].style.opacity = '0.2';
      }
      ul.children[this.animationIndex].style.opacity = '1';
      ul.children[(this.animationIndex + 1) % 12].style.opacity = '0.8';
      ul.children[(this.animationIndex + 2) % 12].style.opacity = '0.6';
      ul.children[(this.animationIndex + 3) % 12].style.opacity = '0.4';
      this.animationIndex = (this.animationIndex + 1) % 12;
    }, 83);

    this.beShow = true;
    return true;
  }

  hide() {
    // 累加器减一
    this.accumulator -= 1;
    if (this.accumulator > 0) {
      return false;
    }
    // 开启滚动
    document.body.style.overflow = '';
    this.beShow = false;
    document.body.removeChild(this.container);
    clearInterval(this.handle);
    clearTimeout(this.handleText);
    this.handle = null;
    this.container = null;
    return true;
  }
}
const loadingInstance = new Loading();
export default loadingInstance;
