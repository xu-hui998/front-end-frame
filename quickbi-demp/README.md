# carinsure

> 自主承保理赔&北京电子化

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

# build for devlop-production with minification
npm run dev-build

# 打包分析
npm run analyze
```

## 启动参数
  NONE

## 项目链接
* 生产外网地址：
  http://220.248.30.68:8154/elecClaimH5/
* 生产内网地址：
  http://9.0.9.153:8080/elecClaimH5/
* 测试外网地址：
  http://220.248.30.68:8154/elecClaimH5Dev/
* 测试内网地址：
  http://9.23.28.81:8080/elec_claim_h5/

## PingPoint
  NONE

## 生产服务部署
### 部署方式
  堡垒机
### 部署账号
  yanghua_guoxin
### 前端服务器
IP：9.23.69.177
    9.23.69.178
    9.23.69.179
    9.23.69.180
路径：/home/weblogic/soft/apache-tomcat-8.5.63/webapps

## 测试部署
### 部署方式
  堡垒机
### 部署账号
  yuanhua
### 前端服务器
IP：9.23.29.196
路径：/home/weblogic/apache-tomcat-8.5.60/webapps

## 全局变量
* messageLevel： 日志输出等级
* requestTimeout： 最长请求时间