module.exports = {
  plugins: {
    autoprefixer: {},
    'postcss-px-to-viewport': {
      exclude: /manageDetail|element-ui/, // 此目录下的文件不会被转换为vm
      unitToConvert: 'px',
      fontViewportUnit: 'vw',
      replace: true,
      viewportWidth: 375, // (Number) The width of the viewport.
      unitPrecision: 5, // (Number) The decimal numbers to allow the REM units to grow to.
      viewportUnit: 'vw', // (String) Expected units.
      propList: ['*', '!font*'], // (Array) The properties that can change from px to vw.
      selectorBlackList: ['.ignore', '.hairlines', '.nnf-'], // (Array) The selectors to ignore and leave as px.
      minPixelValue: 1, // (Number) Set the minimum pixel value to replace.
      mediaQuery: false, // (Boolean) Allow px to be converted in media queries.
    },
    // 过滤::before的样式
    'postcss-viewport-units': {
      filterRule: (rule) => rule.selector.indexOf('::after') === -1 && rule.selector.indexOf('::before') === -1 && rule.selector.indexOf(':after') === -1 && rule.selector.indexOf(':before') === -1,
    },
  },
};
