module.exports = {
  root: true,
  env: {
    node: true,
  },
  globals: {
    "utils": "readonly",
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    "import/no-extraneous-dependencies": ["error", {"devDependencies": true, "optionalDependencies": false, "peerDependencies": false}],
    'no-console': 2,
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'linebreak-style': 'off',
    'prefer-const': 'off',
    "no-unused-vars":"off",
    "no-alert":"off",
    'no-shadow': ["error", { "allow": ["state"] }],
    'max-len' : ["error", {code : 3000}],
    'arrow-parens': 'off', //（关闭圆括号）
  },
};
