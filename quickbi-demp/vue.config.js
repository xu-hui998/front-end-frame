const devWebpackConfig = require('./config/webpack.dev.conf.js');
const prodWebpackConfig = require('./config/webpack.prod.conf.js');

module.exports = function () {
  switch (process.env.NODE_ENV) {
    case 'production':
      return prodWebpackConfig;
    case 'development':
      return devWebpackConfig;
    default:
      return {};
  }
};
