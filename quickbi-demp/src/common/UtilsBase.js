/* eslint-disable func-names */
/**
 * H5页面调用基座方法
 */
import { Dialog } from 'vant';

const UtilsBase = function () {
  const u = navigator.userAgent;
  const isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; // android终端
  const isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); // ios终端
  // 获取用户信息
  const getUserInfo = function (params) {
    if (isAndroid) {
      window.SysClientJs.getUserInfo(JSON.stringify(params));
    } else {
      window.webkit.messageHandlers.getUserInfo.postMessage(JSON.stringify(params));
    }
  };
    // 打开网页
  const openWebviewByUrl = function (params) {
    if (isAndroid) {
      window.SysClientJs.openWebviewByUrl(JSON.stringify(params));
    } else {
      window.webkit.messageHandlers.openWebviewByUrl.postMessage(JSON.stringify(params));
    }
  };

  const gotoPage = function () {
    const params = { type: 'last' };
    if (isAndroid) {
      window.SysClientJs.gotoPage(JSON.stringify(params));
    } else if (isiOS) {
      window.webkit.messageHandlers.gotoPage.postMessage(JSON.stringify(params));
    } else {
      this.$router.push({
        path: `/${this.routeType}`,
        query: {},
      });
    }
  };

  // 离线任务保存
  const offlineTaskSave = function (params) {
    if (isAndroid) {
      window.SysClientJs.offlineTaskSave(JSON.stringify(params));
    } else {
      window.webkit.messageHandlers.offlineTaskSave.postMessage(JSON.stringify(params));
    }
  };
  // 打开影像列表
  const openClaimImagesPage = function (params) {
    if (isAndroid) {
      window.SysClientJs.openClaimImagesPage(JSON.stringify(params));
    } else {
      window.webkit.messageHandlers.openClaimImagesPage.postMessage(JSON.stringify(params));
    }
  };
  // 打开影像补录
  const openSupplementImagePage = function (params) {
    if (isAndroid) {
      window.SysClientJs.openSupplementImagePage(JSON.stringify(params));
    } else {
      window.webkit.messageHandlers.openSupplementImagePage.postMessage(JSON.stringify(params));
    }
  };
  // 是否有影像未上传
  const haveImagesUpload = function (params) {
    if (isAndroid) {
      window.SysClientJs.haveImagesUpload(JSON.stringify(params));
    } else {
      window.webkit.messageHandlers.haveImagesUpload.postMessage(JSON.stringify(params));
    }
  };
  // 任务提交通知
  const taskSubmitNotification = function (params) {
    if (isAndroid) {
      window.SysClientJs.taskSubmitNotification(JSON.stringify(params));
    } else {
      window.webkit.messageHandlers.taskSubmitNotification.postMessage(JSON.stringify(params));
    }
  };
  // 跳转翔创投保详情
  const openInsureDetailPage = function (params) {
    if (isAndroid) {
      window.SysClientJs.openInsureDetailPage(JSON.stringify(params));
    } else {
      Dialog.alert({
        title: '提示',
        message: '该功能只适用于安卓系统，\n请使用安卓手机操作',
        confirmButtonColor: '#009B63',
      });
    }
  };
  // 跳转翔创理赔详情
  const openClaimDetailPage = function (params) {
    if (isAndroid) {
      window.SysClientJs.openClaimDetailPage(JSON.stringify(params));
    } else {
      Dialog.alert({
        title: '提示',
        message: '该功能只适用于安卓系统，\n请使用安卓手机操作',
        confirmButtonColor: '#009B63',
      });
    }
  };

  // 跳转翔创发起理赔流程
  const openClaimTask = function (params) {
    if (isAndroid) {
      window.SysClientJs.openClaimTask(JSON.stringify(params));
    } else {
      Dialog.alert({
        title: '提示',
        message: '该功能只适用于安卓系统，\n请使用安卓手机操作',
        confirmButtonColor: '#009B63',
      });
    }
  };

  // 获取AGS坐标定位点
  const getAGSLocationPoint = function (params) {
    if (isAndroid) {
      window.SysClientJs.getAGSLocationPoint(JSON.stringify(params));
    } else {
      window.webkit.messageHandlers.getAGSLocationPoint.postMessage(JSON.stringify(params));
    }
  };
  // 获取AGS坐标定位点
  const openInsureImagesPage = function (params) {
    if (isAndroid) {
      window.SysClientJs.openInsureImagesPage(JSON.stringify(params));
    } else {
      window.webkit.messageHandlers.openInsureImagesPage.postMessage(JSON.stringify(params));
    }
  };
  return {
    openWebviewByUrl,
    getUserInfo, // 获取用户信息
    gotoPage, // 返回上一页
    offlineTaskSave, // 离线任务保存
    openClaimImagesPage, // 打开影像列表
    openSupplementImagePage, // 影像补录
    haveImagesUpload, // 是否有影像未上传
    taskSubmitNotification, // 任务提交通知
    openInsureDetailPage, // 跳转翔创投保详情（只有安卓）
    openInsureImagesPage, // 跳转影像列表只做展示用
    openClaimDetailPage, // 跳转翔创理赔详情（只有安卓）
    openClaimTask, // 跳转翔创发起理赔流程
    getAGSLocationPoint, // 获取AGS坐标定位点
  };
};
export default UtilsBase();
