import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

// viewport-units-buggyfill vw转px 适配safari
const vub = require('viewport-units-buggyfill');

window.addEventListener('load', () => {
  vub.init({ hacks: window.viewportUnitsBuggyfillHacks });
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
