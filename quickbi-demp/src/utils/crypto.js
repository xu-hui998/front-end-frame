import CryptoJs from 'crypto-js';
import JSEncrypt from 'jsencrypt/bin/jsencrypt';

const LIBRARY = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';// key-library,随机生成指定数量的16进制key时使用
const IV = 'HTMBANKCRYPTOIVV';// 偏移量
const RSAPUBLICKEY = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCc23P4coK2c3Be35g4OXfwbiP8YPa5qbdpn6VKPGbtW8j2UEPTsEByGr4/ureQE9fYAjFudlqJZRe+CeckDUolomQmHbPcZgnm6d+x0wTukFGQlljAPx8IuMhkaaJSfORd2j8MNI3msP90SCjG1Sze884rTXObtnHbsdgKntu8KQIDAQAB';// RSA公钥

// 随机生成指定数量的16进制key
function generateKey(num) {
  let key = '';
  for (let i = 0; i < num; i += 1) {
    const randomPoz = Math.floor(Math.random() * LIBRARY.length);
    key += LIBRARY.substring(randomPoz, randomPoz + 1);
  }
  return key;
}

// AES加密,调用该方法时，传入的data必须是字符串类型,如果要加密对象等类型，需要先用JSON.stringify()将其字符串化再传入
function encryptByAES(data, key) {
  const keyStr = CryptoJs.enc.Utf8.parse(key);
  const dataStr = CryptoJs.enc.Utf8.parse(data);
  const ivStr = CryptoJs.enc.Utf8.parse(IV);
  const encrypted = CryptoJs.AES.encrypt(dataStr, keyStr, {
    iv: ivStr,
    mode: CryptoJs.mode.CBC, // 加密模式，CBC模式   ECB
    padding: CryptoJs.pad.Pkcs7, // 填充方式
  });
  let encryptedBase64Str = encrypted.toString().replace(/\//g, '_');
  encryptedBase64Str = encryptedBase64Str.replace(/\+/g, '-');
  return encryptedBase64Str;
}

// AES解密
function decryptByAES(data, key) {
  // eslint-disable-next-line no-useless-escape
  const vals = data.replace(/\-/g, '+').replace(/_/g, '/');
  const keyStr = CryptoJs.enc.Utf8.parse(key);
  const ivStr = CryptoJs.enc.Utf8.parse(IV);
  const decryptedData = CryptoJs.AES.decrypt(vals, keyStr, {
    iv: ivStr,
    mode: CryptoJs.mode.CBC,
    padding: CryptoJs.pad.Pkcs7,
  });
  const decryptedStr = CryptoJs.enc.Utf8.stringify(decryptedData);
  return decryptedStr;
}

// RSA加密
function encryptByRSA(data, publicKey) {
  const encryptor = new JSEncrypt(); // 新建JSEncrypt对象
  encryptor.setPublicKey(publicKey); // 设置公钥
  const rsaEncryptData = encryptor.encrypt(data); // 对需要加密的数据进行加密
  return rsaEncryptData;
}
const AESKey = generateKey(16);
export const RASKEY = encryptByRSA(AESKey, RSAPUBLICKEY);

export function encryptParams(data) {
  // 加密
  if (!data) {
    return null;
  }
  return encryptByAES(JSON.stringify(data), AESKey);
}
export function decryptParams(data) {
  // 解密
  if (!data) {
    return data;
  }
  return JSON.parse(decryptByAES(data, AESKey));
}
