/* eslint-disable no-use-before-define */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
/* 非空校验 */
export function validateIsNull(str) {
  return str !== '' && str !== null && str !== undefined && str.length !== 0;
}

// 检测是否是Pc端
export function isPC() {
  const userAgentInfo = navigator.userAgent;
  const Agents = ['Android', 'iPhone',
    'SymbianOS', 'Windows Phone',
    'iPad', 'iPod'];
  let flag = true;
  // eslint-disable-next-line no-plusplus
  for (let v = 0; v < Agents.length; v++) {
    if (userAgentInfo.indexOf(Agents[v]) > 0) {
      flag = false;
      break;
    }
  }
  return flag;
}

export function getFormatDate() {
  const date = new Date();
  let month = date.getMonth() + 1;
  let strDate = date.getDate();
  if (month >= 1 && month <= 9) {
    month = `0${month}`;
  }
  if (strDate >= 0 && strDate <= 9) {
    strDate = `0${strDate}`;
  }
  const currentDate = `${date.getFullYear()}-${month}-${strDate
  } ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
  return currentDate;
}
// 查询明天的日期
export function getTomorrowDate() {
  let day = new Date();
  day.setTime(day.getTime() + 24 * 60 * 60 * 1000);
  let date = `${day.getFullYear()}-${day.getMonth() + 1}-${day.getDate() >= 10 ? day.getDate() : `0${day.getDate()}`}`;
  return date;
}

export function checkPhone(phone) {
  const myreg = /^[1][3,4,5,6,7,8][0-9]{9}$/;
  return myreg.test(phone);
}

// 将base64转换为blob
export function dataURLtoBlob(dataurl) {
  const arr = dataurl.split(',');
  const mime = arr[0].match(/:(.*?);/)[1];
  const bstr = atob(arr[1]);
  let n = bstr.length;
  const u8arr = new Uint8Array(n);
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new Blob([u8arr], { type: mime });
}

// 将blob转换为file
export function blobToFile(theBlob, fileName) {
  theBlob.lastModifiedDate = new Date();
  theBlob.name = fileName;
  return theBlob;
}

// 修改日期格式为YYYY-MM-DD
export function exchangeDate(date) {
  const changeDate = date.substring(0, 10);
  return date.substring(0, 10);
}

// 修改日期格式为YYYY年MM月DD日
export function exchangeDateCn(date) {
  return `${date.substring(0, 4)}年${date.substring(5, 7)}月${date.substring(8, 10)}日`;
}

// 获取昨天 返回YYYY年MM月DD日
export function getYeterday(date) {
  const yes = new Date(new Date(date).getTime() - 24 * 60 * 60 * 1000);
  return `${yes.getFullYear()}年${yes.getMonth() + 1}月${yes.getDate()}日`;
}

// 修改日期格式为YYYY年MM月DD日hh时
export function exchangeDateCh(date) {
  return `${date.substring(0, 4)}年${date.substring(5, 7)}月${date.substring(8, 10)}日${date.substring(11, 13)}时`;
}

// 查询一年前的日期
export function beforeDateYear(num) {
  const date = new Date();
  let d = date.getDate();
  let m = date.getMonth() + 1;
  let y = date.getFullYear() - num;
  let newDate = new Date(y - num, m, d - 1);
  m = (m < 10 ? `0${m}` : m);
  d = (d < 10 ? `0${d}` : d);
  let result = (`${y.toString()}-${m.toString()}-${d.toString()}`);
  return result;
}

// 查询上个月的日期
export function beforeDateMonth() {
  let date = new Date();
  let year = date.getFullYear();
  let month = date.getMonth();
  let day = date.getDate();
  if (month === 0) {
    year -= 1;
    month = 12;
  }
  month = (month < 10 ? `0${month}` : month);
  day = (day < 10 ? `0${day}` : day);
  return `${year.toString()}-${month.toString()}-${day.toString()}`;
}

// 银行卡号校验
export function bankCardValidate(bankno) {
  let regx = /^[0-9a-zA-Z]{1,}$/;
  if (!regx.test(bankno)) {
    return false;
  }
  return true;
}

// 银行卡号校验-不适合公账账户银行卡号
export function bankCardValidateDetail(bankno) {
  const lastNum = +bankno.substr(bankno.length - 1, 1);
  const first15Num = bankno.substr(0, bankno.length - 1);
  const newArr = [];
  for (let i = first15Num.length - 1; i > -1; i -= 1) {
    newArr.push(first15Num.substr(i, 1));
  }
  const arrJiShu = [];
  const arrJiShu2 = [];
  const arrOuShu = [];
  for (let j = 0; j < newArr.length; j += 1) {
    if ((j + 1) % 2 === 1) {
      if (parseInt(newArr[j], 10) * 2 < 9) {
        arrJiShu.push(parseInt(newArr[j], 10) * 2);
      } else {
        arrJiShu2.push(parseInt(newArr[j], 10) * 2);
      }
    } else {
      arrOuShu.push(newArr[j]);
    }
  }
  const jishuChild1 = [];
  const jishuChild2 = [];
  for (let h = 0; h < arrJiShu2.length; h += 1) {
    jishuChild1.push(parseInt(arrJiShu2[h], 10) % 10);
    jishuChild2.push(parseInt(arrJiShu2[h], 10) / 10);
  }
  let sumJiShu = 0;
  let sumOuShu = 0;
  let sumJiShuChild1 = 0;
  let sumJiShuChild2 = 0;
  let sumTotal = 0;
  for (let m = 0; m < arrJiShu.length; m += 1) {
    sumJiShu += parseInt(arrJiShu[m], 10);
  }
  for (let n = 0; n < arrOuShu.length; n += 1) {
    sumOuShu += parseInt(arrOuShu[n], 10);
  }
  for (let p = 0; p < jishuChild1.length; p += 1) {
    sumJiShuChild1 += parseInt(jishuChild1[p], 10);
    sumJiShuChild2 += parseInt(jishuChild2[p], 10);
  }
  sumTotal = parseInt(sumJiShu, 10) + parseInt(sumOuShu, 10)
    + parseInt(sumJiShuChild1, 10) + parseInt(sumJiShuChild2, 10);
  const k = parseInt(sumTotal, 10) % 10 === 0 ? 10 : parseInt(sumTotal, 10) % 10;
  const luhn = 10 - k;
  if (lastNum === luhn) {
    return true;
  }
  return false;
}

// 身份证号码校验
export function IdCardValidate(idCard) {
  const idCards = trim(idCard.replace(/ /g, ''));
  if (idCard.length === 15) {
    return isValidityBrithBy15IdCard(idCards);
  } if (idCards.length === 18) {
    const idCardS = idCards.split(''); // 得到身份证数组
    if (isValidityBrithBy18IdCard(idCards) && isTrueValidateCodeBy18IdCard(idCardS)) {
      return true;
    }
    return false;
  }
  return false;

  function isTrueValidateCodeBy18IdCard(aIdCard) {
    const Wi = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1];
    const ValideCode = ['1', '0', '10', '9', '8', '7', '6', '5', '4', '3', '2'];
    let sum = 0;
    const AidCard = aIdCard;
    if (AidCard[17].toLowerCase() === 'x') {
      AidCard[17] = '10';
    }
    for (let i = 0; i < 17; i += 1) {
      sum += Wi[i] * AidCard[i];
    }
    const valCodePosition = sum % 11;
    if (AidCard[17] === ValideCode[valCodePosition]) {
      return true;
    }
    return false;
  }

  function isValidityBrithBy18IdCard(idCard18) {
    const year = idCard18.substring(6, 10);
    const month = idCard18.substring(10, 12);
    const day = idCard18.substring(12, 14);
    const tempDate = new Date(year, parseFloat(month) - 1, parseFloat(day));
    // 这里用getFullYear()获取年份，避免千年虫问题
    if (tempDate.getFullYear()
      !== parseFloat(year)
      || tempDate.getMonth()
      !== parseFloat(month) - 1
      || tempDate.getDate()
      !== parseFloat(day)) {
      return false;
    }
    return true;
  }

  function isValidityBrithBy15IdCard(idCard15) {
    const year = idCard15.substring(6, 8);
    const month = idCard15.substring(8, 10);
    const day = idCard15.substring(10, 12);
    const tempDate = new Date(year, parseFloat(month) - 1, parseFloat(day));
    if (tempDate.getYear()
      !== parseFloat(year)
      || tempDate.getMonth()
      !== parseFloat(month) - 1 || tempDate.getDate() !== parseFloat(day)) {
      return false;
    }
    return true;
  }
  function trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, '');
  }
}

const unitCodeArr = [{
  key: '01',
  value: '件',
}, {
  key: '02',
  value: '个',
}, {
  key: '03',
  value: '箱',
}, {
  key: '04',
  value: '袋',
}, {
  key: '05',
  value: '桶',
}, {
  key: '06',
  value: '包',
}, {
  key: '07',
  value: '盆',
}, {
  key: '08',
  value: '吨',
}, {
  key: '09',
  value: '立方米',
}, {
  key: '10',
  value: '公斤',
}, {
  key: '11',
  value: '亩',
}, {
  key: '12',
  value: '株',
}, {
  key: '13',
  value: '头',
}, {
  key: '14',
  value: '只',
}, {
  key: '15',
  value: '间',
}, {
  key: '16',
  value: '羽',
}, {
  key: '17',
  value: '张',
}, {
  key: '18',
  value: '户',
}, {
  key: '19',
  value: '台',
}, {
  key: '20',
  value: '刀',
}, {
  key: '21',
  value: '匹',
}, {
  key: '22',
  value: 'TEU',
}, {
  key: '23',
  value: '幢',
}, {
  key: '24',
  value: '台账补录',
}, {
  key: '25',
  value: '套',
}, {
  key: '26',
  value: '斤',
}, {
  key: '27',
  value: '千克',
}, {
  key: '28',
  value: '公顷',
}, {
  key: '29',
  value: '千瓦',
}, {
  key: '30',
  value: '马力',
}, {
  key: '31',
  value: '平方米',
}, {
  key: '32',
  value: '条',
}, {
  key: '33',
  value: '尾',
}, {
  key: '34',
  value: '延长米',
}, {
  key: '99',
  value: '其他',
}];
export function changeUnit(val) {
  if (!val) {
    return '';
  }
  let index = unitCodeArr.findIndex(el => el.key === val);
  let result = '';
  if (index !== -1) {
    result = `${unitCodeArr[index].value}、元`;
  }
  return result;
}
