const isiOS = !!navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); // ios终端

function dateStr(date) {
  return isiOS ? date.replaceAll('-', '/') : date;
}

function zeroFormat(num) {
  return num < 10 ? `0${num}` : num;
}

function formatDate(date, type) {
  const y = date.getFullYear();
  const m = date.getMonth() + 1;
  const d = date.getDate();
  const h = date.getHours();
  const min = date.getMinutes();
  const s = date.getSeconds();
  return type
    .replace(/yy/i, zeroFormat(y))
    .replace(/MM/, zeroFormat(m))
    .replace(/dd/i, zeroFormat(d))
    .replace(/hh/i, zeroFormat(h))
    .replace(/mm/, zeroFormat(min))
    .replace(/ss/i, zeroFormat(s));
}

export default {
  dateStr,
  formatDate,
};
