const library = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';// key-library,随机生成指定数量的16进制key时使用
const aesKey = '';// AES加密密钥
const iv = 'HTMBANKCRYPTOIVV';// 偏移量
const rsaPublicKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCc23P4coK2c3Be35g4OXfwbiP8YPa5qbdpn6VKPGbtW8j2UEPTsEByGr4/ureQE9fYAjFudlqJZRe+CeckDUolomQmHbPcZgnm6d+x0wTukFGQlljAPx8IuMhkaaJSfORd2j8MNI3msP90SCjG1Sze884rTXObtnHbsdgKntu8KQIDAQAB';// RSA公钥
const whiteList = ['/app/claimInfo/queryClaims'];// 不添加loading 白名单

export default {
  library,
  aesKey,
  iv,
  rsaPublicKey,
  whiteList,
};
