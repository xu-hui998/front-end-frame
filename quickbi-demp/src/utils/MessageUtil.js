/* eslint-disable no-console */
/**
* @Description: 日志工具类
* @Author: luhan
* @Date: 2021/04/07 10:38:55
* @param: {Number} level 日志等级
* @method: {log} 调试日志输出方法
* @method: {error} 错误日志输出方法
* @return: {number} 成功执行返回当前level, 小于当前日志级别返回-1
*/
import globalConfig from '../../public/config/index.json';

function beShownMessage(level) {
  return level >= globalConfig.messageLevel;
}

export class Message {
  constructor(level = 1) {
    this.level = level;
  }

  log(...msg) {
    if (beShownMessage(this.level)) {
      console.group('message:');
      console.log(...msg);
      console.trace('trace info');
      console.groupEnd();
      return this.level;
    }
    return -1;
  }

  error(...msg) {
    if (beShownMessage(this.level)) {
      console.group('message:');
      console.error(...msg);
      console.trace('trace info');
      console.groupEnd();
      return this.level;
    }
    return -1;
  }
}

export const MessageUtil = new Message();
