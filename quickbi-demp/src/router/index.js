import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [{
  path: '/',
  name: 'incentiveplan',
  meta: {
    title: '厅店激励',
  },
  alias: ['/incentiveplan'],
  component: () => import(/* webpackChunkName: "incentiveplan" */ '../views/incentiveplan/'),
},
{
  path: '/detail/:brand',
  name: 'incentivedetail',
  meta: {
    title: '厅店激励',
  },
  component: () => import(/* webpackChunkName: "incentiveplandetail" */ '../views/incentiveplan/detail'),
},
{
  path: '/noticecenter',
  name: 'noticecenter',
  meta: {
    title: '公告中心自动滚动',
  },
  component: () => import(/* webpackChunkName: "NoticeCenter" */ '../views/Notice/NoticeCenter.vue'),
},
{
  path: '/notice',
  name: 'notice',
  meta: {
    title: '公告中心手动滑动',
  },
  component: () => import(/* webpackChunkName: "Notice" */ '../views/Notice/Notice.vue'),
},
{
  path: '/ordeprovince',
  name: 'order',
  meta: {
    title: '分公司排名',
  },
  component: () => import(/* webpackChunkName: "BrandRanking" */ '../views/Rank/BrandRanking.vue'),
},
{
  path: '/test',
  name: 'test',
  meta: {
    title: '分公司排名测试',
  },
  component: () => import(/* webpackChunkName: "BrandRanking" */ '../views/Rank/RankingTest.vue'),
},
{
  path: '/orderbrand',
  name: 'order',
  meta: {
    title: '品牌排名',
  },
  component: () => import(/* webpackChunkName: "RankingOfBranches" */ '../views/Rank/RankingOfBranches.vue'),
}, {
  path: '/plan/:mode*',
  name: 'plan',
  meta: {
    title: '计划总结',
  },
  component: () => import(/* webpackChunkName: "plan" */ '../views/plan/index.vue'),
}, {
  path: '/passengerflowanalysis',
  name: 'passengerflowanalysis',
  meta: {
    title: '客流分析',
  },
  component: () => import(/* webpackChunkName: "passengerflowanalysis" */ '../views/passengerflowanalysis/index.vue'),
},

];

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

export default router;
