import provinceData from './province.json';

export default provinceData.map((el, index) => ({
  count: (100000 + Math.random() * 1000000).toFixed(0),
  contry: el,
  date: {
    day: (5000 + Math.random() * 100000).toFixed(0),
    month: (500000 + Math.random() * 100000).toFixed(0),
    year: (1000000 + Math.random() * 10000000).toFixed(0),
    dayCount: (1000 + Math.random() * 10000).toFixed(2),
    monthCount: (5000 + Math.random() * 10000).toFixed(2),
    yearCount: (80000 + Math.random() * 100000).toFixed(2),
    ratio: (Math.random() * 100).toFixed(2),
    lead: (Math.random() * 100).toFixed(2),
  },
}))
  .sort((a, b) => b.count - a.count)
  .map((ele, index) => {
    let tmp = ele;
    tmp.id = index + 1;
    tmp.order = index + 1;
    return tmp;
  });
