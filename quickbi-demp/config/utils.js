exports.getProcessArgv = (function () {
  const argvMap = new Map();
  const argvArr = process.argv.slice(3);
  argvArr.forEach((el) => {
    const tmp = el.replace(/^-+/, '').split('=');
    argvMap.set(tmp[0], tmp[1]);
  });
  return argvMap;
}());
