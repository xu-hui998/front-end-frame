const { merge } = require('webpack-merge');
const utils = require('./utils');
const baseWebpackConfig = require('./webpack.base.conf');

const webpackConfig = merge(baseWebpackConfig, {
  configureWebpack: {
    devtool: process.env.ENV_CHUNKS_VCONSOLE === 'true' ? 'hidden-source-map' : false,
    externals: {
      'bn.js': 'bn.js',
    },
  },
  chainWebpack: (config) => {
    if (utils.getProcessArgv.has('analyze')) {
      // eslint-disable-next-line global-require
      const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
      config.plugin('BundleAnalyzerPlugin').use(BundleAnalyzerPlugin);
    }
  },
});

module.exports = webpackConfig;
