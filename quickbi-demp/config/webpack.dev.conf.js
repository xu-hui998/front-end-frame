const { merge } = require('webpack-merge');
const baseWebpackConfig = require('./webpack.base.conf');

const webpackConfig = merge(baseWebpackConfig, {
  devServer: {
    // host: 'localhost',
    // port: 8080,
    proxy: { // 配置跨域
      '/proxy': {
        target: process.env.PROXY_URL,
        ws: true,
        changOrigin: true,
        pathRewrite: {
          '^/proxy': '',
        },
      },
    },
  },
});

module.exports = webpackConfig;
