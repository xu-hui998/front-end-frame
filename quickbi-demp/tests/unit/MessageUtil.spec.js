import { MessageUtil, Message} from '../../src/utils/MessageUtil';
const assert = require('assert');

describe('MessageUtil.js', () => {
  it('Output message to console And return current level 1 when passed', () => {
    assert.strictEqual(MessageUtil.log('message log test'), 1);
  });
  it('Output message to Message And return current level 2 when passed', () => {
    let message = new Message(2)
    assert.strictEqual(message.log('message log test'), 2);
  });
  it('Output error message to console And return current level 3 when passed', () => {
    let message = new Message(3)
    assert.strictEqual(message.error('message log test'), 3);
  });
  it("Don't output message to console And return -1 when passed", () => {
    let message = new Message(0)
    assert.strictEqual(message.log('message log test'), -1);
  });
});
